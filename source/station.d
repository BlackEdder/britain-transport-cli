import std.algorithm : joiner, filter, map, multiSort, sort, uniq;
import std.conv : to;
import std.json : parseJSON, JSONValue;
import std.net.curl : get;
import std.range : array;
import std.regex : replaceAll, regex;
import std.stdio : writeln;
import std.typecons : Tuple;

import docopt;

import painlessjson.painlessjson;

import transportapi.key;
import transportapi.journey;
import transportapi.stop;
import transportapi.url;

import search;

void main( string[] args )
{
    auto doc = "Station

        Usage:
        station NAME

        Options:
        -h --help     Show this screen.

        Program to get information about public transport station. It will search for the given name and return matching stations, including their latitude and identification code.
        ";

    auto arguments = docopt.docopt(doc, args[1..$], true, "Station");

    // Get lon/lat
    Location[] locs;
    string searchName = arguments["NAME"].to!string;
    if ( "NAME" in arguments )
    {
        auto url = "http://nominatim.openstreetmap.org/search?format=json&countrycodes=gb&q=" ~ searchName;

        locs = parseJSON(get( url.replaceInvalidCharacters )).array
            .map!( (js) => Location( js["lat"].str().to!double,
                        js["lon"].str().to!double ) )
            .array;
    }

    Stop[] stops;

    // Get stations near lon lat for each mode (train/tube/bus)
    foreach( type; [["bus","stops"],["train","stations"],["tube","stations"]] )
    {
        foreach( loc; locs )
        {
            auto url = "http://transportapi.com/v3/uk/"~type[0] ~"/" 
                    ~ type[1] ~"/near.json?" 
                    ~ toUrl(loc) ~ "&"
                    ~ toUrl(readKey);
            auto js = parseJSON( get( url ) );
            stops ~= js[type[1]].fromJSON!(Stop[]);
        }
    }

    auto weightedStops = stops.map!( s => Tuple!(Stop, double)( s, 
                weightSearchSentence( searchName, s.name ) ) )
        .array;
        //.sort!("a[0].mode<b[0].mode")
        //.sort!("a[0].stopID<b[0].stopID")
        //.sort!("a[1]<b[1]")
    weightedStops.multiSort!("a[1]<b[1]",
                    "a[0].stopID < b[0].stopID",
                    "a[0].mode < b[0].mode"
                    );
    weightedStops.uniq 
        //.uniq!((a,b) => 
        //        a[0].mode == b[0].mode && a[0].stopID == b[0].stopID )
        .filter!( "a[1] > 0.01" )
        .map!( (t) => t[0].toJSON!Stop.to!string )
        .joiner( "\n" )
        .writeln;
}
