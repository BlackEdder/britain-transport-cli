import std.algorithm : filter, reduce, sort, joiner, max;
import std.array : array, isArray, back, front, popFront, empty;
import std.conv : to;
import std.file : exists;
import std.json : parseJSON;
import std.range : isForwardRange, walkLength;
import std.stdio : File, writeln;
import std.typecons : Tuple;

import painlessjson.painlessjson;

import docopt;

import transportapi.journey;
import transportapi.live;
import transportapi.stop;

import search;
import fitting;

alias DepartureTuple = Tuple!(
        DateTime, string, string, string, TimeOfDay, string);

auto debugRange(R)(R inRange) 
{
    import std.range : isForwardRange, array;
    import std.stdio : stderr;
    static if (isForwardRange!R) 
    {
        auto range = inRange.save;
    } else {
        auto range = inRange.array;
    }

    auto copy = range;
    stderr.writeln( "Debugging a range" );

    foreach( el; range )
        stderr.writeln( el );
    return copy;
}

auto groupBy(alias func, R)(R values) if (isInputRange!R)
{
    alias K = typeof(func(values.front));
    alias V = ElementType!R[];
    V[K] grouped;
    foreach (value; values)
        grouped[func(value)] ~= value;
    return grouped;
}

unittest
{
    struct Test
    {
        string a;
        double b;
    }

    auto values = [Test("a", 1), Test("a", 2), Test("b", 3)];
    auto grouped = values.groupBy!((a) => a.a);
    assert(grouped["a"].length == 2);
    assert(grouped["a"][1].b == 2);
    assert(grouped["b"].length == 1);
    assert(grouped["b"][0].b == 3);
}

auto removeDuplicates(R)(R r)
{
    import std.algorithm : multiSort, uniq;
    import std.range : zip;

    r.multiSort!( "a[1]<b[1]", "a[2]<b[2]", "a[0]<b[0]", "a[4]<b[4]" )();

    auto sampleTimes = r.map!("a[0]").uniq;
    DepartureTuple[] onTheWay;
    DepartureTuple[] arrived;

    while( !r.empty )
    {
        auto currentTime = sampleTimes.front;
        sampleTimes.popFront;
        DepartureTuple[] sampledBuses;
        while( !r.empty && r.front[0] == currentTime ) 
        {
            // Can't trust these results
            if ((r.front[4]-r.front[0].timeOfDay) < dur!"minutes"(45))
            {
                sampledBuses ~= r.front;
            }
            r.popFront;
        }
        if (onTheWay.length == 0)
        {
            onTheWay = sampledBuses;
        } else if ( !sampledBuses.empty ) {
            // If the differences in predicted arrival time
            // are smallest when we skip the first n step -> 
            // count those as arrived (all the rest are now onTheWay).
            auto bestI = onTheWay.length;
            //auto minDuration = dur!"minutes"(max( 5, 5*sampledBuses.length));
            auto minDuration = 10.0;
            foreach( i; 0..onTheWay.length )
            {
                auto zipped = zip( onTheWay[i..$], sampledBuses[0..$] );
                auto dim = zipped.walkLength.to!double;
                auto averageDuration =
                    zipped.map!((a)
                            {
                                auto dur = a[0][4]-a[1][4];
                                if (dur.isNegative)
                                    dur = -dur;
                                return dur;
                            }
                            )
                    .reduce!((a,b) => a+b).total!"minutes"/dim;
                if (averageDuration < minDuration)
                {
                    bestI = i;
                    minDuration = averageDuration;
                }
            }
            if (bestI > 0)
                arrived ~= onTheWay[0..bestI];
            onTheWay = sampledBuses;
        }
    }

    arrived.map!( (t) => 
            t[0].to!string ~ ", " ~
            t[1].to!string ~ ", " ~
            t[2].to!string ~ ", " ~
            t[3].to!string ~ ", " ~
            t[4].to!string )
        .joiner( "\n" )
        .writeln;

    return arrived;
}

auto matchingDeparturesByLocation( in Location loc,
        RoutePart routepart,
        Stop[] knownStops, DepartureTuple[] departures )
{
    // Match by mode, then sort by distance then take everything in order of
    // distance, for each of thos look through our
    // live data.
    auto stopsByDistance = knownStops.sort!((a,b) => 
            distance(a.location, loc) <
            distance(b.location, loc)
            );

    //stopsByDistance.writeln;
    //departures.filter!( (a) => a[1] == "490000094B" ).writeln;
    
    foreach( st; stopsByDistance )
    {
        auto matchingDetails = departures.filter!(
                (a) => a[1] == st.stopID &&
                a[2] == routepart.lineName && a[5] == routepart.mode &&
                weightSearchSentence( a[3], routepart.destination )> 0.8 );
        if (matchingDetails.walkLength>0) {
            return matchingDetails;
        }
    }
    assert(0, "No matching details found");
}


void main( string[] args )
{
    auto doc = "Analysis

        Usage:
        analysis

        Options:
        -h --help     Show this screen.

        Program to analyse journeys and their average length/variability in length. 
        ";

    auto arguments = docopt.docopt(doc, args[1..$], true, "Journey");

	auto fileNameJourneys = "data/journeys.json";
    auto fileNameLive = "data/livetimes.json";
    auto fileNameStops = "data/stations.json";
	if (!exists( fileNameJourneys ) 
            || !exists( fileNameStops )
            || !exists( fileNameLive ) )
    {
        assert(0, "Files not found");
    }

    auto journeyPlan = File( fileNameJourneys, "r" )
        .byLine
        .map!( line => line.parseJSON.fromJSON!JourneyPlan );

    //journeyPlan.writeln;

    auto liveDepartures = File( fileNameLive, "r" )
        .byLine
        .map!( line => line.parseJSON.fromJSON!StopDepartures );

    //liveDepartures.writeln;
    DepartureTuple[] departureDetails;
    foreach( depR; 
            liveDepartures.map!( (sd) 
                {
                return sd.departures.map!( (dep) => 
                        DepartureTuple(
                            sd.timeOfRequest, sd.stopID,
                            dep.line, dep.direction, dep.timeEstimate, dep.mode ) ).array;
                } ) )
    {
        departureDetails ~= depR;
    }

    // Choose a journey, pick start, end, bus identifier (number and destination)
    // Note destination is sometimes slightly different, so use a weighing, not exact
    // match
    auto routepart = journeyPlan.front.routes.front.routeParts.front;


    auto stops = 
        File( fileNameStops, "r" )
        .byLine
        .map!( line => line.parseJSON.fromJSON!Stop ).array;

    auto fromDetails = matchingDeparturesByLocation( 
            routepart.waypoints.front, routepart, stops,
            departureDetails ).array.removeDuplicates;
    auto toDetails = matchingDeparturesByLocation( 
            routepart.waypoints.back, routepart, stops,
            departureDetails ).array.removeDuplicates;


   fit(fromDetails, toDetails).writeln;

    // Two more things needed, first break all results if more than hour no data
    // Mean time function based on live journey information.
}
