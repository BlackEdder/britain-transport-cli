module transportapi.journey;

import std.datetime : Duration, dur;
import std.json : JSONValue, parseJSON;
import dunit.toolkit;

import painlessjson.painlessjson;

import transportapi.json;

version (unittest)
{
    import std.stdio : writeln;

    auto exampleJourney = 
q{{"request_time":"2015-03-18T07:16:09+00:00","source":"TfL journey planning API","acknowledgements":"Transport for London", "routes":[ {"duration":"00:42:00","route_parts":[ {"mode":"foot","from_point_name":"TW8 0BB","to_point_name":"Albany Road (TW8)","destination":"","line_name":"","duration":"00:03:00","departure_time":"07:22","arrival_time":"07:25","coordinates":[ [ -0.30108,51.48497],[-0.3012,51.48513],[-0.30107,51.48516],[-0.30078,51.48522],[-0.29939,51.48569],[-0.29965,51.4861],[-0.29972,51.4862],[-0.29979,51.48618]]}, {"mode":"bus","from_point_name":"Albany Road (TW8)","to_point_name":"Ealing Broadway Station","destination":"Ealing Broadway","line_name":"65","duration":"00:22:00","departure_time":"07:25","arrival_time":"07:47","coordinates":[ [ -0.29972,51.4862],[-0.2999,51.48648],[-0.3002,51.48694],[-0.30045,51.48741],[-0.30064,51.48783],[-0.30073,51.488],[-0.30117,51.48856],[-0.3013,51.4887],[-0.3013,51.4887],[-0.3016,51.48906],[-0.30184,51.48922],[-0.30232,51.48956],[-0.30256,51.48972],[-0.3031,51.49008],[-0.30341,51.49036],[-0.30344,51.49039],[-0.30355,51.49051],[-0.30371,51.49071],[-0.30528,51.50566],[-0.30515,51.50631],[-0.30506,51.50696],[-0.30507,51.50741],[-0.30509,51.50752],[-0.30519,51.50807],[-0.3052,51.50816],[-0.30522,51.50847],[-0.30535,51.5092],[-0.30536,51.50929],[-0.30585,51.51303],[-0.30436,51.51321],[-0.30451,51.51426],[-0.30466,51.51522],[-0.30474,51.51599],[-0.30466,51.51599],[-0.30417,51.51608],[-0.30371,51.51617],[-0.30291,51.51623],[-0.30272,51.51623],[-0.30239,51.51625],[-0.30213,51.51625],[-0.30204,51.51612],[-0.30226,51.51598],[-0.30228,51.51585],[-0.3024,51.51569]]}, {"mode":"train","from_point_name":"Ealing Broadway","to_point_name":"London Paddington","destination":"London Paddington","line_name":"22750","duration":"00:12:00","departure_time":"07:52","arrival_time":"08:04","coordinates":[ [ -0.30174,51.51469],[-0.29773,51.51489],[-0.29538,51.51494],[-0.29195,51.51489],[-0.28836,51.51483],[-0.2864,51.51487],[-0.28406,51.51498],[-0.2824,51.51504],[-0.27772,51.51535],[-0.27465,51.5157],[-0.27197,51.51611],[-0.26712,51.51706],[-0.26712,51.51706],[-0.26647,51.51718],[-0.26485,51.51754],[-0.26366,51.51783],[-0.26188,51.51836],[-0.25988,51.51903],[-0.25618,51.52048],[-0.25296,51.52179],[-0.25186,51.52216],[-0.24947,51.52288],[-0.24701,51.52351],[-0.2439,51.52428],[-0.23974,51.52509],[-0.23711,51.52544],[-0.23554,51.52557],[-0.22931,51.52572],[-0.22738,51.52574],[-0.22512,51.5257],[-0.22346,51.52565],[-0.21968,51.52529],[-0.21789,51.52507],[-0.2154,51.52472],[-0.21415,51.52451],[-0.21126,51.52391],[-0.20811,51.5231],[-0.20226,51.52139],[-0.1846,51.51859],[-0.18316,51.51864],[-0.20075,51.52115],[-0.19501,51.5198],[-0.19339,51.51952],[-0.19112,51.51918],[-0.18891,51.51895],[-0.18316,51.51864],[-0.18105,51.51852],[-0.18021,51.51845],[-0.17974,51.51836],[-0.17912,51.51811],[-0.17861,51.51781],[-0.1774,51.51699]]}],"departure_time":"07:22","arrival_time":"08:04"}, {"duration":"00:43:00","route_parts":[ {"mode":"foot","from_point_name":"TW8 0BB","to_point_name":"Albany Road (TW8)","destination":"","line_name":"","duration":"00:03:00","departure_time":"07:29","arrival_time":"07:32","coordinates":[ [ -0.30108,51.48497],[-0.3012,51.48513],[-0.30107,51.48516],[-0.30078,51.48522],[-0.29939,51.48569],[-0.29965,51.4861],[-0.29972,51.4862],[-0.29979,51.48618]]}, {"mode":"bus","from_point_name":"Albany Road (TW8)","to_point_name":"Ealing Broadway Station","destination":"Ealing Broadway","line_name":"65","duration":"00:23:00","departure_time":"07:32","arrival_time":"07:55","coordinates":[ [ -0.29972,51.4862],[-0.2999,51.48648],[-0.3002,51.48694],[-0.30045,51.48741],[-0.30064,51.48783],[-0.30073,51.488],[-0.30117,51.48856],[-0.3013,51.4887],[-0.3013,51.4887],[-0.3016,51.48906],[-0.30184,51.48922],[-0.30232,51.48956],[-0.30256,51.48972],[-0.3031,51.49008],[-0.30341,51.49036],[-0.30344,51.49039],[-0.30355,51.49051],[-0.30371,51.49071],[-0.30528,51.50566],[-0.30515,51.50631],[-0.30506,51.50696],[-0.30507,51.50741],[-0.30509,51.50752],[-0.30519,51.50807],[-0.3052,51.50816],[-0.30522,51.50847],[-0.30535,51.5092],[-0.30536,51.50929],[-0.30585,51.51303],[-0.30436,51.51321],[-0.30451,51.51426],[-0.30466,51.51522],[-0.30474,51.51599],[-0.30466,51.51599],[-0.30417,51.51608],[-0.30371,51.51617],[-0.30291,51.51623],[-0.30272,51.51623],[-0.30239,51.51625],[-0.30213,51.51625],[-0.30204,51.51612],[-0.30226,51.51598],[-0.30228,51.51585],[-0.3024,51.51569]]},{"mode":"train","from_point_name":"Ealing Broadway","to_point_name":"London Paddington","destination":"London Paddington","line_name":"21677","duration":"00:13:00","departure_time":"07:59","arrival_time":"08:12","coordinates":[[-0.30174,51.51469],[-0.29773,51.51489],[-0.29538,51.51494],[-0.29195,51.51489],[-0.28836,51.51483],[-0.2864,51.51487],[-0.28406,51.51498],[-0.2824,51.51504],[-0.27772,51.51535],[-0.27465,51.5157],[-0.27197,51.51611],[-0.26712,51.51706],[-0.26712,51.51706],[-0.26647,51.51718],[-0.26485,51.51754],[-0.26366,51.51783],[-0.26188,51.51836],[-0.25988,51.51903],[-0.25618,51.52048],[-0.25296,51.52179],[-0.25186,51.52216],[-0.24947,51.52288],[-0.24701,51.52351],[-0.2439,51.52428],[-0.23974,51.52509],[-0.23711,51.52544],[-0.23554,51.52557],[-0.22931,51.52572],[-0.22738,51.52574],[-0.22512,51.5257],[-0.22346,51.52565],[-0.21968,51.52529],[-0.21789,51.52507],[-0.2154,51.52472],[-0.21415,51.52451],[-0.21126,51.52391],[-0.20811,51.5231],[-0.20226,51.52139],[-0.1846,51.51859],[-0.18316,51.51864],[-0.20075,51.52115],[-0.19501,51.5198],[-0.19339,51.51952],[-0.19112,51.51918],[-0.18891,51.51895],[-0.18316,51.51864],[-0.18105,51.51852],[-0.18021,51.51845],[-0.17974,51.51836],[-0.17912,51.51811],[-0.17861,51.51781],[-0.1774,51.51699]]}],"departure_time":"07:29","arrival_time":"08:12"}]}};
}

struct Location
{
    double lat;
    double lon;

    this( in double[] coords )
    {
        lon = coords[0];
        lat = coords[1];
    }

    this( double latitude, double longitude )
    {
        lat = latitude;
        lon = longitude;
    }
}

string toUrl(T:Location)( in T loc )
{
    import std.conv : to;
    return "lat=" ~ loc.lat.to!string ~ "&lon=" ~ loc.lon.to!string;
}

double toRadians( in double degrees )
{
    import std.math : PI;
    return degrees*PI/180;
}

///
double distance( in Location fromLoc, in Location toLoc )
{
    import std.conv : to;
    import std.math : cos, sin, atan2, sqrt;
    auto earthRadius = 6371000.0;
    auto lat1 = fromLoc.lat.toRadians;
    auto lat2 = toLoc.lat.toRadians;
    auto deltaLat = (fromLoc.lat - toLoc.lat)
        .toRadians;
    auto deltaLon = (fromLoc.lon - toLoc.lon)
        .toRadians;

    auto a = sin(deltaLat/2)*sin(deltaLat/2) + 
        cos(lat1)*cos(lat2)*sin(deltaLon/2)*sin(deltaLon/2);
    return earthRadius* 2.0 * atan2(sqrt(a),sqrt(1.0-a));
}

unittest
{
    assertApprox( distance( Location( 51.4861, -0.29847 ),
                Location( 51.4864, -0.29732 ) ), 86.3327, 0.01 );
    assertApprox( distance( Location( 51.4864, -0.29732 ),
                Location( 51.4861, -0.29847 ) ), 86.3327, 0.01 );
}

struct RoutePart
{
    string mode;
    string lineName;
    Duration duration;
    string fromPointName;
    string toPointName;
    string destination;
    Location[] waypoints;

    static RoutePart _fromJSON( in JSONValue json ) 
    {
        import std.array : array;
        RoutePart rp = defaultFromJSON!RoutePart( json );
        rp.duration = durationFromJSON( json["duration"] );

        rp.waypoints = json["coordinates"].fromJSON!(double[][])
            .map!((a) => Location(a)).array;
        return rp;
    }
}

double distance( in RoutePart rp )
{
    import std.algorithm : sum;
    import std.range : iota;
    import std.array : array;

    return iota( 0, rp.waypoints.length-1, 1 )
        .map!( (i) => distance( rp.waypoints[i], rp.waypoints[i+1] ) )
        .sum;
}

struct Route
{
    RoutePart[] routeParts;
}

double distance( in Route r )
{
    import std.algorithm : sum;
    return r.routeParts.map!( (rp) => rp.distance ).sum;
}

struct JourneyPlan
{
    import std.datetime : DateTime;
    Route[] routes;
    DateTime timeOfRequest;

    static JourneyPlan _fromJSON( in JSONValue json ) 
    {
        JourneyPlan jp = defaultFromJSON!JourneyPlan(json);
        jp.timeOfRequest = DateTime.fromISOExtString(
                json["request_time"].str()[0..19] );
        return jp;
    }
}

unittest
{
    auto exampleJSON = parseJSON( exampleJourney );
    auto jplan = fromJSON!JourneyPlan( exampleJSON );
    assertEqual( jplan.routes.length, 2 );
}

unittest
{
    auto exampleJSON = parseJSON( exampleJourney );
    auto jplan = fromJSON!JourneyPlan( exampleJSON );
    assertApprox(jplan.routes[0].routeParts[0].waypoints[0].lon, -0.30108 );
    assertApprox( distance( jplan.routes[0].routeParts[0] ), 
            226.395, 0.01 );
    assertApprox( distance( jplan.routes[0] ), 
            15506.9, 0.01 );

}


