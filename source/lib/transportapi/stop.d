module transportapi.stop;

import std.json : JSONValue;
import painlessjson.painlessjson : defaultFromJSON, fromJSON, toJSON;

version (unittest)
{
    import std.json : parseJSON;
    import std.stdio : writeln;

    import dunit.toolkit;

    auto exampleStopBus = q{{"atcocode":"490000125D","smscode":"59524","name":"Kew Gardens","mode":"bus","bearing":"N","locality":"Kew","indicator":"Stop N","longitude":-0.28684,"latitude":51.47783,"distance":172}};
    auto exampleStopTube = q{{"station_code":"KEW","atcocode":"9400ZZLUKWG","name":"Kew Gardens","mode":"tube","longitude":-0.28524,"latitude":51.47706,"lines":["district"],"distance":365}};
    auto exampleStopTrain = q{{"station_code":"VIC","atcocode":null,"tiploc_code":"VICTRIC","name":"London Victoria","mode":"train","longitude":-0.144559,"latitude":51.495256,"distance":1419}};
}

struct Stop
{
    import transportapi.journey : Location;
    string name;
    string mode;
    string stopID;
    Location location;

    static Stop _fromJSON( in JSONValue json ) 
    {
        Stop stop = defaultFromJSON!Stop( json );
        if ("stopID" in json)
            stop.stopID = json["stopID"].fromJSON!string;
        else if ( stop.mode == "bus" )
            stop.stopID = json["atcocode"].fromJSON!string;
        else
            stop.stopID = json["station_code"].fromJSON!string;
        if ("latitude" in json)
        {
            stop.location = Location( json["latitude"].fromJSON!double,
                    json["longitude"].fromJSON!double );
        }
        return stop;
    }
}

unittest
{
    assertEqual( parseJSON( exampleStopBus ).fromJSON!(Stop).name,
            "Kew Gardens" );
    assertEqual( parseJSON( exampleStopBus ).fromJSON!(Stop).stopID,
            "490000125D" );
    assertEqual( parseJSON( exampleStopTube ).fromJSON!(Stop).stopID,
            "KEW" );
    assertEqual( parseJSON( exampleStopTrain ).fromJSON!(Stop).stopID,
            "VIC" );
}
