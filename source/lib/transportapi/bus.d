module transportapi.bus;

import std.json : parseJSON;
import dunit.toolkit;
import painlessjson.painlessjson;

struct Bus
{
    string line;
}

unittest
{
    string js = "{\"mode\":\"bus\",\"line\":\"65\",\"direction\":\"Ealing Broadway (Ealing Broadway)\",\"operator\":\"LONDONBUS\",\"aimed_departure_time\":\"09:15\",\"dir\":\"outbound\",\"date\":\"2015-03-17\",\"source\":\"Traveline\"}";
    Bus bus = fromJSON!Bus( parseJSON( js ) );
    assertEqual( bus.line, "65" );
}
