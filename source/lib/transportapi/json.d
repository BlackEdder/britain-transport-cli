module transportapi.json;

import std.json : JSONValue;

import std.datetime : dur, Duration, TimeOfDay;

TimeOfDay timeFromJSON( in JSONValue json )
{
    import std.algorithm : splitter;
    import std.array : array;
    import std.conv : to;
    auto spl = json.str().splitter(":").array;
    return TimeOfDay( spl[0].to!int, spl[1].to!int );
}

Duration durationFromJSON( in JSONValue json )
{
    import std.algorithm : splitter;
    import std.array : array;
    import std.conv : to;
    auto durationSplit = json.str().splitter(":").array;
    return dur!"hours"( durationSplit[0].to!int ) +
        dur!"minutes"( durationSplit[1].to!int ) +
        dur!"seconds"( durationSplit[2].to!int );
}


