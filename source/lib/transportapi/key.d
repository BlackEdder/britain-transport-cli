module transportapi.key;

import std.stdio : File, writeln, stderr;
import std.file : exists, expandTilde;
import std.json : parseJSON;
import std.string : format;

import painlessjson.painlessjson;

version( unittest )
{
    import std.stdio : writeln;

    import dunit.toolkit;
}

struct ApiKey 
{
    string api_key;
    string app_id;
}

unittest
{
    auto apiKey = fromJSON!ApiKey( 
            parseJSON( q{{ "api_key": "key", "app_id": "id" }}) );
    assertEqual( apiKey.api_key, "key" );
    assertEqual( apiKey.app_id, "id" );
}

string toUrl(T:ApiKey)( in T apiKey )
{
    return format( "api_key=%s&app_id=%s", apiKey.api_key, apiKey.app_id );
}

unittest
{
    auto apiKey = fromJSON!ApiKey( 
            parseJSON( q{{ "api_key": "key", "app_id": "id" }}) );
    assertEqual( apiKey.toUrl, "api_key=key&app_id=id" );
}

string readFile( string path, string name ) {
	auto fileName = path ~ "/" ~ name;
	string content;
	if (exists( fileName )) {
		File file = File( fileName, "r" );
		foreach ( line; file.byLine())
			content ~= line;
	} else {
        stderr.writeln( "File does not exist " ~ fileName ); 
    }
	return content;
}

ApiKey readKey()
{
    import std.stdio : writeln;
    string fname = "key.json";
	auto content = readFile( expandTilde( "~/.config/britaintransportcli" ), 
            fname );
    return fromJSON!ApiKey( 
            parseJSON( content ) );
}
