module transportapi.live;

import std.datetime : TimeOfDay, DateTime;
import std.json : JSONValue;
import painlessjson.painlessjson : defaultFromJSON, fromJSON;

import transportapi.json;

struct Departure
{
    string mode;
    string line;
    string direction;
    TimeOfDay timeEstimate;

    static Departure _fromJSON( in JSONValue json )
    {
        Departure dep = defaultFromJSON!(Departure)(json);
        dep.timeEstimate = 
            json["best_departure_estimate"]
            .timeFromJSON;
        return dep;
    }
}

struct StopDepartures 
{
    string stopID;
    Departure[] departures;
    DateTime timeOfRequest;

    static StopDepartures _fromJSON( in JSONValue json ) 
    {
        StopDepartures stop;
        foreach( k, v; json["departures"].object )
            stop.departures ~= v.fromJSON!(Departure[]);
        stop.stopID = json["atcocode"].fromJSON!string;
        stop.timeOfRequest = DateTime.fromISOExtString(
                json["request_time"].str()[0..19] );
            //.fromJSON!string.replaceAll( regex(r"\+\d\d:\d\d"),""));
        return stop;
    }
}
