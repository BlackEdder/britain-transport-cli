module transportapi.url;

version(unittest)
{
    import dunit.toolkit;
}

/// Replace invalid characters (i.e. space) in url
string replaceInvalidCharacters( string url )
{
    import std.regex : regex, replaceAll;
    return url.replaceAll(regex(" "),"%20");
}

unittest
{
    assertEqual( replaceInvalidCharacters( "bla bla" ), "bla%20bla" );
}
