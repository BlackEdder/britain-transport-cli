module fitting;

import std.datetime : dur, Duration;

/// mu is not log yet
real dLogNormal( real x, real mu, real sd )
{
    import std.math : log, sqrt, exp, PI, pow;
    if (x<=0 || mu <= 0 || sd <= 0)
        return 0;
    return 1.0/(x*sd*sqrt(2*PI))*exp(
            -(pow(log(x)-log(mu),2))/(2.0*pow(sd,2)));

}

/// Give times, do the skip myself
auto probability(R)( R fromTimes, R toTimes, 
        real meanTime, real sdTime ) 
{
    import std.algorithm : min, map, reduce;
    import std.conv : to;
    import std.range : empty, zip, walkLength;
    import std.math : pow;
    if (fromTimes.empty || toTimes.empty)
        return 0;
    auto dim = min(fromTimes.walkLength, toTimes.walkLength);
    return zip(fromTimes, toTimes).map!( (timeTuple) 
            {
                return dLogNormal(
                        (timeTuple[1]-timeTuple[0]).total!"minutes".to!real,
                        meanTime, sdTime );
            })
        .reduce!((a,b) => a*b).pow(1.0/dim);
}

auto groupDetailsByTime(R)( R toGroup, Duration gap = dur!"minutes"(60) )
{
    import std.range : empty, front, back, popFront;
    R[] grouped;
    auto prevTime = toGroup.front[0];
    grouped = [[toGroup.front]];
    toGroup.popFront;
    while (!toGroup.empty)
    {
        if (toGroup.front[0]-prevTime > gap)
            grouped ~= [[toGroup.front]];
        else
            grouped.back ~= toGroup.front;

        prevTime = toGroup.front[0];
        toGroup.popFront;
    }
    return grouped;
}

auto fit(R)(R fromDetails, R toDetails, int tripLength = 7)
{
    import std.algorithm : map;
    import std.range : walkLength, zip;
    import std.array : array;
    import std.conv : to;

    R[] groupedFromDetails = groupDetailsByTime(fromDetails);
    R[] groupedToDetails = groupDetailsByTime(toDetails);

    return zip(groupedFromDetails, groupedToDetails).map!( (detailsTuple) 
            {
            auto initFunction = delegate double[]()
            {
            import std.random : uniform;
            return [ uniform( 0.0, detailsTuple[1].walkLength ), uniform( 0.0, 10.0 )];
            };

            auto fn = ( double[] pars ) {
            import std.math : round;
            auto i = pars[0].round.to!int;
            if (i < 0 || i >= detailsTuple[1].walkLength)
            return 0;
            return -probability( detailsTuple[0].map!"a[4]".array[0..$],
                    detailsTuple[1].map!"a[4]".array[i..$], tripLength, pars[1] ).to!double;
            };

            import minimized;
            auto de = new DifferentialEvolution!(double[])();
            de.temperatureFunction = fn;
            de.randomIndividual = initFunction;
            return de.minimize[1];
            });
}
