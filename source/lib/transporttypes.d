module transporttypes;

import std.math : pow, sqrt, log;
import std.random : Random, rndGen;

import dstats.random : rExponential, rLogNorm;
import dstats.summary : meanStdev;

version( unittest )
{
    import std.stdio : writeln;
    import dunit.toolkit;
}

double rLogNormStd(RGen = Random)( double mean, double sd, ref RGen gen = rndGen )
{
    assert( sd > 0 );
    double v = pow(sd, 2);
    double part = (1.0+v/pow(mean,2));
    double mu = log( mean/sqrt( part ) );
    double sigma = sqrt( log( part ) );
    return rLogNorm!RGen( mu, sigma );
}

unittest
{
    auto gen = Random( 1 );
    double[100] samples;
    foreach( i; 0..100 )
    {
        samples[i] = rLogNormStd( 10, 0.3, gen );
    }

    auto res = samples.meanStdev;
    assertApprox( res.mean, 10, 0.02 );
    assertApprox( res.stdev, 0.3, 0.1 );
}

interface Transport
{
    string getDescription();


    double timeTillDeparture( double time );
    double transitTime();

}

class Train : Transport
{
    string description = "train";


    double departureTime; // first departure time 
    double intervalTime; // Each interval
    double averageDelay; // Exponential after departure

    double transitTimeMean; // Log Normal distributed
    double transitTimeSD;

    string getDescription()
    {
        return description;
    }

    double timeTillDeparture( double time )
    {
       return minTime( time ) + rExponential( averageDelay );
    }

    double transitTime()
    {
        return rLogNormStd( transitTimeMean, transitTimeSD );
    }

    private:

    double minTime( double time )
    {
        if (departureTime > time)
            return departureTime;
        double minTime = intervalTime - (time-departureTime)%intervalTime;
        if (minTime == intervalTime)
            minTime = 0;
        return minTime;
    }
}

unittest
{
    auto train = new Train;
    train.departureTime = 5;
    train.intervalTime = 15;
    assertEqual( train.minTime( 0 ), 5 );
    assertEqual( train.minTime( 5 ), 0 );
    assertEqual( train.minTime( 20 ), 0 );
    assertEqual( train.minTime( 34 ), 1 );
}

class Bus : Transport
{
    string description = "bus";

    double arrivalRate; // Exponential

    double transitTimeMean; // Log Normal distributed
    double transitTimeSD;

    string getDescription()
    {
        return description;
    }

    double timeTillDeparture( double time )
    {
       return rExponential( arrivalRate );
    }

    double transitTime()
    {
        return rLogNormStd( transitTimeMean, transitTimeSD );
    }
}

class Underground : Transport
{
    string description = "underground";

    double arrivalRate; // Exponential 

    double transitTimeMean; // Log Normal distributed
    double transitTimeSD;

    string getDescription()
    {
        return description;
    }

    double timeTillDeparture( double time )
    {
       return rExponential( arrivalRate );
    }

    double transitTime()
    {
        return rLogNormStd( transitTimeMean, transitTimeSD );
    }
}
