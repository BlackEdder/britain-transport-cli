import std.conv : to;
import std.net.curl : get;
import std.stdio : writeln;

import painlessjson.painlessjson : fromJSON;

import transportapi.key;
import transportapi.url;

import docopt;

void main( string[] args )
{
    auto doc = "Journey

        Usage:
        journey FROMSTOP TOSTOP

        Options:
        -h --help     Show this screen.

        Program to get information about journeys on public transport. Stops should correspond to names of the stop.
        ";

    auto arguments = docopt.docopt(doc, args[1..$], true, "Journey");

    auto url = "http://transportapi.com/v3/uk/public/journey/from/stop:"
            ~ arguments["FROMSTOP"].to!string ~ "/to/stop:"
            ~ arguments["TOSTOP"].to!string
            ~ ".json?"
            ~ toUrl( readKey );

    get( url.replaceInvalidCharacters )
        .writeln;
}
