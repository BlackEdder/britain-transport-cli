import std.conv : to;
import std.net.curl : get;
import std.stdio : writeln;

import painlessjson.painlessjson : fromJSON;

import transportapi.key;
import transportapi.url;

import docopt;

void main( string[] args )
{
    auto doc = "Live

        Usage:
        live --type TYPE STOPID

        Options:
        -h --help     Show this screen.
        --type TYPE   Transport type (bus/train/tube)

        Program to get information about live public transport.
        ";

    auto arguments = docopt.docopt(doc, args[1..$], true, "Live");

    auto url = "http://transportapi.com/v3/uk/"
            ~ arguments["--type"].to!string ~ "/stop/"
            ~ arguments["STOPID"].to!string
            ~ "/live.json?"
            ~ toUrl( readKey );

    get( url.replaceInvalidCharacters )
        .writeln;
}
