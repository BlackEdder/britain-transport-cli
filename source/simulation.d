import std.algorithm : map;
import std.conv : to;
import std.string : join;
import std.random : uniform;
import std.stdio : writeln;

import transporttypes;


void main() 
{
    auto train = new Train;
    train.departureTime = 0;
    train.intervalTime = 15;
    train.averageDelay = 2;
    train.transitTimeMean = 15;
    train.transitTimeSD = 1;

    //writeln( "#plotcli -o train -b 0,35,0,400" );

    foreach( i; 0..1000 )
    {
        auto time = 0.0;

        time += train.timeTillDeparture( time );
        time += train.transitTime;
        writeln( time );
    }

    auto bus = new Bus;
    bus.arrivalRate = 10;
    bus.transitTimeMean = 15;
    bus.transitTimeSD = 4;

    //writeln( "#plotcli -o bus -b 0,35,0,400" );

    foreach( i; 0..1000 )
    {
        auto time = 0.0;

        time += bus.timeTillDeparture( time );
        time += bus.transitTime;
        writeln( time );
    }

    auto underground = new Underground;
    underground.arrivalRate = 6;
    underground.transitTimeMean = 15;
    underground.transitTimeSD = 1;

    //writeln( "#plotcli -o underground -b 0,35,0,400" );
    writeln( "#plotcli -o underground" );

    foreach( i; 0..1000 )
    {
        auto time = 0.0;

        time += underground.timeTillDeparture( time );
        time += underground.transitTime;
        writeln( time );
    }

    /*
       Test all combinations with a start time randomly between 0 and 15
       ( to make it even for trains ) 
       do delete this start time at the end
       */
    Transport[] transports = [
        cast(Transport) train, 
        cast(Transport) underground,
        cast(Transport) bus
            ];

    double[][] traces;
    string[] names;
    writeln( "#plotcli -d b,b,.. -o combined " );
    foreach( i; 0..10000 )
    //foreach( i; 0..20 )
    {
        traces ~= [[]];
        foreach( transport1; transports )
        {
            foreach( transport2; transports )
            {
                if (i == 0)
                    names ~= transport1.getDescription ~ "_" ~ 
                        transport2.getDescription;
                /*writeln( "#plotcli -d b -o ", transport1.getDescription, "_",
                  transport2.getDescription );*/
                //transport2.getDescription, " -b 0,60,0,1500" );
                auto startTime = 0;

                if (transport1.getDescription == "train")
                    (cast(Train)(transport1)).departureTime = uniform( 0.0, 15 );
                if (transport2.getDescription == "train")
                    (cast(Train)(transport2)).departureTime = uniform( 0.0, 15 );


                auto time = startTime;

                time += transport1.timeTillDeparture( time );
                time += transport1.transitTime;
                time += transport2.timeTillDeparture( time );
                time += transport2.transitTime;

                traces[traces.length-1] ~= [time - startTime];
            }
        }
    }
    traces.map!( (row) => row.map!(to!string).join( "," ) ).join("\n").writeln;
    names.writeln;

}
